import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
Vue.prototype.$axios = axios
Vue.config.productionTip = false
Vue.config.errorHandler = function (err, vm, info) {
 // console.log('err:',err)
 // console.log('vm',vm)
 // console.log('info',info)
 console.log(err,vm,info)
}
new Vue({
  render: h => h(App),
}).$mount('#app')
