module.exports = {
	lintOnSave:false,
	devServer: {
		// Paths
		proxy: {
			'/api': {  //使用"/api"来代替"http://f.apiplus.c" 
				target: 'http://192.168.0.146:9000', //源地址 
				changeOrigin: true, //改变源 
				ws:true,
				pathRewrite: {
					'^/api': '' //路径重写 
				}
			}
		}
	}
} 