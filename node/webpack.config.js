const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');//css合并

const devMode = process.env.NODE_ENV !== 'production';
const webpack = require('webpack')
//entry 可以是单个 也可以是对象
module.exports = {
    entry: {
        home: "./webpack/src/qwe.js",
        // css: "./webpack/src/css.scss"
    },
    output: {
        path: path.resolve(__dirname, "./webpack/dist"),
        filename: "[name].build.js"//name是出口的名字
    },
    mode: "development",//是否压缩文件   目前是非压缩的  便于调试
    module: {
        rules: [
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            options: {
                                publicPath: '../',
                                hmr: process.env.NODE_ENV === 'development',
                                reloadAll: true,
                            },
                        }
                    },
                    'css-loader',
                    // 'postcss-loader',
                    'sass-loader',
                ],
            },
            {//压缩图片
                test: /\.(png|jpg|gif)$/i,//文件类型
                use: {
                    loader: "url-loader",//使用的loader
                    options: {//配置
                        limit: 8192//小于8KB被压缩
                    }
                }
            },
            {//ES6转ES5
                test: /\.js$/,//筛选出.js
                exclude: /(node_modules|bower_components)/,//排除这两个文件夹
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jquery: 'jquery'
        }),
        new MiniCssExtractPlugin({
            filename: './css/[name]-build.css',
            chunkFilename: '[id].css'
        })
    ]

};


//file-loader 读取文件
//url-loader 转化为base url
//css-loader 打包css
