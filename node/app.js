const express = require('express')
const bodyParser = require('body-parser')
const app = express();
const fs = require('fs')
const router =require('./routers/router.js')
const session = require('express-session')

app.use(session({
    secret: 'lueluelue',//在原有的加密基础上加上这个字符串再进行加密   增加安全性的  防止客户端恶意伪造
    resave: false,
    saveUninitialized: false//为true时无论你是否存入session 都会默认分配给你一把钥匙  false 为不默认分配
}))

app.use(express.static('public'));



app.use(bodyParser.urlencoded({extended: false}))

// parse application/json
app.use(bodyParser.json());

app.use(router);
app.use(function (req, res) {
    fs.readFile('./views/404.html','utf8',function (err, data) {
        if (err) {
            return console.log('404页面读取失败')
        }
        res.send(data)
    })
});
app.listen(9000, () => {
    console.log('启动成功le');
});
